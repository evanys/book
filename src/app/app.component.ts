import { Component } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[ApiService]
})
export class AppComponent {
  title = 'Contasic';
  planes = [{ccod_cue: '', cdsc:''}]

  constructor(private api:ApiService){
    this.getallPlanes();
    
  }

  getallPlanes=()=>{
    this.api.allPlans().subscribe(
      data=>{
        this.planes = data;
      },
      error=>{
        console.log(error, 'error en GetAllPlanes');
      }
      
    )
  }

}
