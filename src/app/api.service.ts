import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  baseUrl= "http://hernancapcha.webfactional.com";

  private headers = new HttpHeaders({'Content-Type': 'application/json',
'Access-Control-Allow-Origin': '*'});


  constructor(private http: HttpClient) { }

  allPlans():Observable<any>{
    return this.http.get(this.baseUrl+'/plan/', {headers:this.headers});
  }

}
